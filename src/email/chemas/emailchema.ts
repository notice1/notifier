import * as mongoose from 'mongoose';

export const Emailchema = new mongoose.Schema({
    text: String,
    header: String,
    userId: Number,
    email: String,
    status: String
});