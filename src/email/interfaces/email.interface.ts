export interface Email {
    _id: string,
    header: string,
    text: string,
    userId: number
}