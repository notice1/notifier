import {Body, Controller, Get, HttpException, Post} from '@nestjs/common';
import {ResultDto} from "../dto/result.dto";
import {EmailService} from "./email.service";
import {PushEmailDto} from "./dto/push-email.dto";

@Controller('email')
export class EmailController {
    constructor(private emailService: EmailService) {}

    @Post()
    async pushEmail(@Body() pushEmailDto: PushEmailDto): Promise<ResultDto>  {
        const email = await this.emailService.create(pushEmailDto);

        if (!email) {
            throw new HttpException('Fail send email', 500);
        }

        return {
            message: email,
            data: ''
        }
    }
}
