import {Module} from '@nestjs/common';
import {EmailController} from './email.controller';
import {EmailService} from './email.service';
import {Emailchema} from "./chemas/emailchema";
import {MongooseModule} from "@nestjs/mongoose";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Email', schema: Emailchema}])
    ],
    controllers: [EmailController],
    providers: [EmailService]
})
export class EmailModule {
}
