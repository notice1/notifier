import {IsArray, IsEmail, IsJSON, IsNotEmpty} from "class-validator";

export class PushEmailDto {
    html: string;

    header: string;

    @IsNotEmpty()
    text: string;

    @IsEmail()
    email: string;

    @IsNotEmpty()
    userId: number;
}
