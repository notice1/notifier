import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {PushEmailDto} from "./dto/push-email.dto";
import {Email} from "./interfaces/email.interface";
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class EmailService {
    constructor(
        @InjectModel('Email') private emailModel: Model<any>,
        private readonly mailerService: MailerService
    ) {}

    async create(pushEmailDto: PushEmailDto): Promise <any> {
        const status = await this.pushEmail(pushEmailDto);

        const email = new this.emailModel({
            ...pushEmailDto,
            status: status
        });

        email.save();

        return status;
    }

    async pushEmail(pushEmailDto: PushEmailDto): Promise <any> {
        try {
            await this.mailerService.sendMail({
                to: pushEmailDto.email, // list of receivers
                from: 'fosteev4@ya.ru', // sender address
                subject: 'Testing Nest MailerModule ✔', // Subject line
                text: pushEmailDto.text, // plaintext body
                html: pushEmailDto.html, // HTML body content
            });

            return 'Success';
        } catch (e) {
            return e.message;
        }
    }
}
