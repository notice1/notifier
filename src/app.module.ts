import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {EmailModule} from './email/email.module';
import {MailerModule} from '@nestjs-modules/mailer';
import {MongooseModule} from "@nestjs/mongoose";

@Module({
    imports: [
        EmailModule,
        MongooseModule.forRoot('mongodb://localhost/notifier'),
        MailerModule.forRootAsync({
            useFactory: () => ({
                transport: {
                    host: 'smtp.gmail.com',
                    port: 465,
                    secure: true, // upgrade later with STARTTLS
                    auth: {
                        user: "fosteevworking@gmail.com",
                        pass: "Fynbdpkjv5364",
                    },
                    defaults: {
                        from: '"nest-modules" <modules@nestjs.com>',
                    },
                    template: {},
                }
            })
        }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
